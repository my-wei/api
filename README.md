# My Wei - API

## Running this project

Clone this repo: `git clone https://gitlab.com/my-wei/api.git 'My Wei Api'`

Install node modules: `yarn`

Start Postgress server: `docker-compose up -d`

Migrate the database `yarn migrate`

Seed the database `yarn seed`

## Sequelize ORM

[Link to docs](http://docs.sequelizejs.com/)

Creating a model: `sequelize model:generate --name Device --attributes guid:string,version:string`

## Heroku

Deploying: `git push heroku master`

Bash: `heroku run bash`

Logs: `heroku logs`

Set timezone: `heroku config:add TZ="Europe/Brussels"`

## HTTP Requests

### GET Readings

Parameters:
- **uuid:** c1bca200-be5a-4725-b430-c5b2d72a1d60
- **reading:** solarPanelVoltage, batteryVoltage, waterTemperature

Query parameters:
- **timeSpan:** day, week
- **date**: YYYY-MM-HH
- **utcOffset**: number between -16 & 16

Example
```
/api/readings/c1bca200-be5a-4725-b430-c5b2d72a1d60/solarPanelVoltage?timeSpan=week&date=2019-04-19&utcOffset=2
```

### POST Reading

Parameters:
- **uuid:** c1bca200-be5a-4725-b430-c5b2d72a1d60

Body:
- **solarPanelVoltage:** float
- **batteryVoltage:** float
- **waterTemperature:** float

Example
```
/api/readings/c1bca200-be5a-4725-b430-c5b2d72a1d60
{
	"solarPanelVoltage": 9.23,
	"batteryVoltage": 7.54,
	"waterTemperature": 20.1
}
```
