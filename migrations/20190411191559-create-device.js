'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
      .then(() => {
        return queryInterface.createTable('Devices', {
          uuid: {
            type: Sequelize.UUID,
            primaryKey: true,
            defaultValue: Sequelize.literal('uuid_generate_v4()'),
            allowNull: false,
            autoIncrement: false,
          },
          firmwareVersion: {
            type: Sequelize.STRING
          },
          createdAt: {
            allowNull: false,
            defaultValue: new Date(),
            type: Sequelize.DATE
          },
          updatedAt: {
            allowNull: false,
            defaultValue: new Date(),
            type: Sequelize.DATE
          }
        });
      });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Devices');
  }
};
