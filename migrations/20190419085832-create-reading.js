'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Readings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      solarPanelVoltage: {
        type: Sequelize.FLOAT
      },
      batteryVoltage: {
        type: Sequelize.FLOAT
      },
      waterTemperature: {
        type: Sequelize.FLOAT
      },
      deviceUuid: {
        type: Sequelize.UUID,
        references: {
          model: 'Devices',
          key: 'uuid'
        },
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Readings');
  }
};