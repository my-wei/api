'use strict';
module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define('Device', {
    uuid: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    firmwareVersion: DataTypes.STRING
  }, {});
  Device.associate = function(models) {
    Device.hasMany(models.Reading, {foreignKey: 'deviceUuid'});
  };
  return Device;
};
