'use strict';
module.exports = (sequelize, DataTypes) => {
  const ExpoDevice = sequelize.define('ExpoDevice', {
    token: DataTypes.STRING,
    myWeiType: {
      type: DataTypes.STRING
    },
  }, {});
  ExpoDevice.associate = function(models) {
    // associations can be defined here
  };
  return ExpoDevice;
};
