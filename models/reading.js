'use strict';
module.exports = (sequelize, DataTypes) => {
  const Reading = sequelize.define('Reading', {
    solarPanelVoltage: DataTypes.FLOAT,
    batteryVoltage: DataTypes.FLOAT,
    waterTemperature: DataTypes.FLOAT
  }, {});
  Reading.associate = function(models) {
    Reading.belongsTo(models.Device, {foreignKey: 'deviceUuid'});
  };
  return Reading;
};