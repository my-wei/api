const express = require('express');
const router = express.Router();
const models = require('../models');
const config = require('../config/config');
const { Expo } = require('expo-server-sdk');

router.post('/register', async function(req, res, next) {
  if(!req.body) {
    res.status(400).send('No body attached');
    return;
  }
  if(!req.body.token) {
    res.status(400).send('No token in body');
    return;
  }

  const token = req.body.token;

  if (!Expo.isExpoPushToken(token)) {
    res.status(400).send('Invalid Expo token');
    return;
  }

  try {
    await models.ExpoDevice.create(req.body);
  } catch(error) {
    res.status(400).send('Can\'t read body');
    return;
  }

  let expo = new Expo();
  try {
    await expo.sendPushNotificationsAsync([{
      to: token,
      sound: 'default',
      body: 'When the battery drops below ' + config.myWei.battery_voltage_alarm + 'V you will receive a new notification',
      data: { myWeiType: req.body.myWeiType },
    }]);
  } catch(error) {
    res.status(503).send('Error during Expo ticket request');
  }

  res.status(200).send();
});

router.post('/unregister', async function(req, res, next) {
  if(!req.body) {
    res.status(400).send('No body attached');
    return;
  }
  if(!req.body.token) {
    res.status(400).send('No token in body');
    return;
  }

  const token = req.body.token;

  if (!Expo.isExpoPushToken(token)) {
    res.status(400).send('Invalid Expo token');
    return;
  }

  try {
    await models.ExpoDevice.destroy({
      where: {
        token: token,
        myWeiType: req.body.myWeiType
      }
    });
  } catch(error) {
    res.status(400).send('Can\'t read body');
    return;
  }

  res.status(200).send();
});

router.get('/registrations/:token', async function(req, res, next) {
  if(!req.params.token) {
    res.status(400).send('No token parameter');
    return;
  }

  const token = req.params.token;

  if (!Expo.isExpoPushToken(token)) {
    res.status(400).send('Invalid Expo token');
    return;
  }

  try {
    let myWeiTypes = await models.ExpoDevice.findAll({
      attributes: ['myWeiType'],
      where: {
        token: token
      },
      group: ['myWeiType'],
      raw : true,
    });
    myWeiTypes = myWeiTypes.map((object) => object.myWeiType);
    return res.status(200).json(myWeiTypes);
  } catch(error) {
    res.status(400).send('Can\'t read body');
  }
});

module.exports = router;
