const express = require('express');
const router = express.Router();
const models = require('../models');
const moment = require('moment');
const { Expo } = require('expo-server-sdk');
const config = require('../config/config');

router.get('/:uuid/:reading', async function (req, res) {
  let date = moment.utc({
    year: moment.utc().year(),
    month: moment.utc().month(),
    day: moment.utc().date()
  });

  if (req.query.date) {
      date = moment.utc(req.query.date);
      if (!date.isValid()) {
        res.status(400).send('Date format not recognised');
        return;
      }
  }

  if (req.query.utcOffset) {
    const utcOffset = +req.query.utcOffset;
    if (utcOffset && utcOffset > -16 && utcOffset < 16) {
      date.utcOffset(utcOffset);
    } else {
      res.status(400).send('Incorrect UTC offset');
      return;
    }
  }

  let options;

  if (req.query.timeSpan === 'week') {
    options = {
      attributes: [
        [models.sequelize.fn('AVG', models.sequelize.col(req.params.reading)), 'value'],
        [models.sequelize.fn('date_trunc', 'day', models.sequelize.col('createdAt')), 'dateTime']
      ],
      where: {
        deviceUuid: req.params.uuid,
        createdAt: {
          [models.Sequelize.Op.lt]: date.endOf('isoWeek').format(),
          [models.Sequelize.Op.gt]: date.startOf('isoWeek').format()
        }
      },
      order: [models.sequelize.fn('date_trunc', 'day', models.sequelize.col('createdAt'))],
      group: ['dateTime']
    };
    models.Reading.findAll(options)
      .then((readings) => res.status(200).json(readings))
      .catch(() => res.status(400).send('Invalid UUID'));
  } else {
    options = {
      attributes: [
        [req.params.reading, 'value'],
        ['createdAt', 'dateTime']
      ],
      where: {
        deviceUuid: req.params.uuid,
        createdAt: {
          [models.Sequelize.Op.lt]: date.endOf('day').format(),
          [models.Sequelize.Op.gt]: date.startOf('day').format()
        }
      },
      order: ['createdAt'],
    };
    models.Reading.findAll(options)
      .catch(() => res.status(400).send('Invalid UUID'))
      .then((readings) => readings.map((reading) => {
        reading.dateTimeAtForHour = moment.utc(reading.dataValues.dateTime).startOf('hour').format();
        return reading;
      }))
      .then(readings =>
        [...new Set(readings.map(r => r.dateTimeAtForHour))]
          .map(hour => readings.slice().reverse().find(r => moment.utc(r.dateTimeAtForHour).isSame(moment.utc(hour))))
      ).then((readings) => res.status(200).json(readings));
  }
});

router.post('/:uuid', async function (req, res) {
  if (!req.body) {
    res.status(400).send('No body attached');
    return;
  }

  if (!req.body.batteryVoltage) {
    res.status(400).send('No body attached');
    return;
  }

  const batteryVoltage = +req.body.batteryVoltage;

  if (batteryVoltage > 0 && batteryVoltage < config.myWei.battery_voltage_alarm) {
    try {
      const subscribers = await models.ExpoDevice.findAll({
        attributes: ['token'],
        where: {
          myWeiType: 'batteryVoltage'
        },
        raw: true,
      });
      const messages = subscribers.map(subscriber => {
        return {
          to: subscriber.token,
          sound: 'default',
          body: 'The battery has gone below ' + config.myWei.battery_voltage_alarm + 'V',
          data: { myWeiType: 'batteryVoltage' },
        }
      });
      const expo = new Expo();
      await expo.sendPushNotificationsAsync(messages);
    } catch(error) {
      console.log(error);
    }
  }

  models.Device.findByPk(req.params.uuid)
    .then((device) => {
      if(!device) {
        res.status(400).send('UUID unregistered');
        return;
      }
      const newReading = {...req.body, deviceUuid: req.params.uuid};
      return models.Reading.create(newReading)
        .then(() => res.status(201).send())
        .catch(() => res.status(400).send('Can\'t read body'));
    })
    .catch(() => res.status(400).json('Invalid UUID'));
});

module.exports = router;
