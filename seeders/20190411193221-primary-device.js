'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Devices', [{
        uuid: 'c1bca200-be5a-4725-b430-c5b2d72a1d60',
        firmwareVersion: '1.0'
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Devices', null, {});
  }
};
